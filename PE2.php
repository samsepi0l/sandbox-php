<?php
$pre = 0;
$cur = 1;
$nex = 0;

$s = 0;

$cap = 4000000;

while ($cur <= $cap) {
	$nex = $pre + $cur;
	$pre = $cur;
	$cur = $nex;

	if ($cur % 2 == 0) {
		$s = $s + $cur;
	}
}

echo $s . "\n";

?>
