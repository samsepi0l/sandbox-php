<?php

$c = 0;

$n = 1;

while ($c < 20) {
	for ($i = 1; $i <= 20; $i++) {
		if ($n % $i == 0) {
			$c++;
		} else {
			$c = 0;
			break;
		}
	}

	if ($c == 20) {
		echo $n . "\n";
		break;
	} else {
		$n++;
	}
}

?>
